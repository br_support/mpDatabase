PROGRAM _INIT
(* Insert code here *)
	MpDatabaseCore_0.MpLink := ADR(gDatabaseCore);
	MpDatabaseCore_0.Enable := TRUE;
	//	MpDatabaseCore_0.Connect := TRUE;
	MpDatabaseCore_0();
		
	QueryName1 := 'select all';
	QueryName2 := 'select all 2';
	QueryName3 := 'select all 3';
	
	MpDatabaseQuery_0.MpLink := ADR(gDatabaseCore);
	MpDatabaseQuery_0.Name := ADR(QueryName1);
	MpDatabaseQuery_0.Enable := TRUE;
	MpDatabaseQuery_0();

	MpDatabaseQuery_1.MpLink := ADR(gDatabaseCore);
	MpDatabaseQuery_1.Name := ADR(QueryName2);
	MpDatabaseQuery_1.Enable := TRUE;
	MpDatabaseQuery_1();

	MpDatabaseQuery_2.MpLink := ADR(gDatabaseCore);
	MpDatabaseQuery_2.Name := ADR(QueryName3);
	MpDatabaseQuery_2.Enable := TRUE;
	MpDatabaseQuery_2();
	
	tableName := 'support';
	
	QueryArray1;
	QueryArray2;
	QueryArray3;
	
END_PROGRAM

PROGRAM _CYCLIC
	//get actual time
	DTGetTime_0(enable := TRUE);
	Query.FinishTime := DTGetTime_0.DT1;

	
	MpDatabaseCore_0();
	
	MpDatabaseQuery_0(Execute := TON_0.Q);
	IF MpDatabaseQuery_0.CommandDone THEN
		TON_0(IN := FALSE);
	END_IF
	
	MpDatabaseQuery_1(Execute := TON_1.Q);
	IF MpDatabaseQuery_1.CommandDone THEN
		TON_1(IN := FALSE);
	END_IF

	MpDatabaseQuery_2(Execute := TON_2.Q);
	IF MpDatabaseQuery_2.CommandDone THEN
		TON_2(IN := FALSE);
	END_IF
	
	TON_0(IN := MpDatabaseCore_0.Connected, PT := T#2.5s);
	TON_1(IN := MpDatabaseCore_0.Connected, PT := T#2.6s);
	TON_2(IN := MpDatabaseCore_0.Connected, PT := T#2.7s);



END_PROGRAM

PROGRAM _EXIT
	MpDatabaseCore_0(Enable := FALSE);
	
	MpDatabaseQuery_0(Enable := FALSE);
	MpDatabaseQuery_1(Enable := FALSE);	
	MpDatabaseQuery_2(Enable := FALSE);
	
END_PROGRAM